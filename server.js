require('dotenv').config({path:'./.env'})
const express = require("express");
const bodyParser = require("body-parser");
const connectDB = require("./config/db.config");
const dnaController = require("./controllers/dna.controller");

const app = express();

// database
connectDB();

// configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// routes
app.route(`/stats`).get(dnaController.get);
app.route(`/mutation`).post(dnaController.create);

// server
app.listen(process.env.PORT, () => {
  console.log(`Prueba Candidatos | Server listening on port ${process.env.PORT}`);
});

module.exports = app;
