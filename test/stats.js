let mongoose = require("mongoose");
let DNA = require("../models/dna.model");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../server");
let DNAService = require("../services/dna.service");
let should = chai.should();
var assert = require("assert");

process.env.NODE_ENV = "test";

chai.use(chaiHttp);

let dnas = require("./dnas.json");

describe("Mutations Service", () => {
  dnas.filter(dna=>dna.isValid).forEach((dna) => {
    it(
      dna.hasMutation
        ? "it should be a mutation"
        : "it should NOT be a mutation",
      (done) => {
        let hasMutation = DNAService.hasMutation(dna.value);
        hasMutation.should.be.eql(dna.hasMutation);
        done();
      }
    );
  });
});

describe("Mutations API", () => {
  DNA.deleteMany({}, (err) => {
    done();
  });
  
  describe("/GET stats", () => {
    it("it should GET an empty mutation stats", (done) => {
      chai
        .request(server)
        .get("/stats")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          res.body.should.to.eql({
            count_mutations: 0,
            count_no_mutation: 0,
            ratio: 1,
          });
          done();
        });
    });
  });

  describe("/POST mutation", () => {
    dnas.forEach((dna) => {
      it(
        dna.isValid
          ? "it should POST an invalid mutation":
        dna.hasMutation
          ? "it should POST a mutation"
          : "it should POST a NO mutation",
        (done) => {
          chai
            .request(server)
            .post("/mutation")
            .send({dna: dna.value})
            .end((err, res) => {
              if (!dna.isValid) res.should.have.status(400);
              else if (dna.hasMutation) res.should.have.status(200);
              else res.should.have.status(403);
              done();
            });
        }
      );
    });
  });

  describe("/GET stats", () => {
    it("it should GET the mutation stats", (done) => {
      chai
        .request(server)
        .get("/stats")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          res.body.should.to.eql({
            count_mutations: 4,
            count_no_mutation: 4,
            ratio: 1,
          }); 
          done();
        });
    });
  });
});
