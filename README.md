# README.MD
El objetivo del proyecto es exponer una API Rest con funciones necesarias para detectar si  una persona tiene diferencias genéticas basándose en su secuencia de ADN.

## Contenido
El proyecto contiene el **servicio de mutaciones** (services/dna.service.js), la implementaciòn de una **API Rest** que expone los servicios necesarios y un **test** tanto sobre el servicio como la API.

## Demo
Se puede probar las llamadas a la API en la siguiente URL. https://pruebacandidatosnode.uc.r.appspot.com

Ejemplos de llamadas:

GET /stats HTTP/1.1
Host: pruebacandidatosnode.uc.r.appspot.com

POST /mutation HTTP/1.1
Host: pruebacandidatosnode.uc.r.appspot.com
Content-Type: application/json

{"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]}


## Instalación
Para instalar el servicor, tanto para desarrollo como para produccion, debe descargar el codigo y ejecutar el siguiente comando en la carpeta raiz:

```bash
npm install
```

Se debe cambiar la configuración correspondiente al ambiente de instalacion (puerto y base de datos) en el archivo .env.

Para iniciar el servidor debe ejecutar el siguiente comando en la carpeta raiz:
```bash
npm run server
```

## Ejecución de test
Los datos utilizados en el testing se encuentran en  /test/dnas.json.
Para ejecutar las pruebas se debe correr el comando:

```bash
npm run test
```
