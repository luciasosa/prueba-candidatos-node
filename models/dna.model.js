const mongoose = require('mongoose');

const DNASchema = new mongoose.Schema({
  data: {
    type: [String],
    required: true
  },
  hasMutation: {
    type: Boolean,
    required: true
  }
});

module.exports = mongoose.model('dna', DNASchema);
