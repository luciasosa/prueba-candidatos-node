const DNAService = require("../services/dna.service");
const DNA = require("../models/dna.model");

async function create(req, res) {
  //Co^^E&$eeM!^0y()[_]Rbb
  try {
    const { dna } = req.body;
    if (!DNAService.isValid(dna))
      return res.status(400).json({ message: "Invalid DNA" });
    const hasMutation = DNAService.hasMutation(dna);
    const newDna = new DNA({
      data: dna,
      hasMutation,
    });
    await newDna.save();
    if (hasMutation) return res.sendStatus(200);
    else return res.sendStatus(403);
  } catch {
    return res.sendStatus(500);
  }
}
module.exports.create = create;

async function get(req, res) {
  try {
    const count_mutations = await DNA.countDocuments({ hasMutation: true });
    const count_no_mutation = await DNA.countDocuments({ hasMutation: false });
    const ratio = Math.trunc((count_no_mutation == 0 ? 1 : count_mutations / count_no_mutation)*10)/10;
    return res.status(200).json({ count_mutations, count_no_mutation, ratio });
  } catch(error) {
    console.error(error);
    return res.sendStatus(500); 
  }
}
module.exports.get = get;