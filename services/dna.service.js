const utils = require("../utils/isEmpty");

module.exports.hasMutation = function (dna) {
  /* dna.forEach((element) => {
    console.log(element);
  }); */
  let counter = 0;
  let y = 0;
  let exclude = [[], [], []];
  while (counter < 2 && y < dna.length) {
    let x = 0;
    while (counter < 2 && x < dna.length) {
      [exclude, a, b, d] = hasMutationElement(dna, exclude, x, y);
      //console.log(exclude, x, y);
      counter = counter + a + b + d;
      x++;
    }
    y++;
  }

  return counter >= 2;
};

const isElementUsed = function (exclude, x, y) {
  return exclude.some((e) => {
    return e[0] == x, e[1] == y;
  });
};

const hasMutationElement = function (dna, exclude, x, y) {
  let mh = isElementUsed(exclude[0], x, y)
    ? 0
    : x >= dna.length - 3
    ? 0
    : dna[y][x] == dna[y][x + 1] &&
      dna[y][x] == dna[y][x + 2] &&
      dna[y][x] == dna[y][x + 3]
    ? 1
    : 0;

  let mv = isElementUsed(exclude[1], x, y)
    ? 0
    : y >= dna.length - 3
    ? 0
    : dna[y][x] == dna[y + 1][x] &&
      dna[y][x] == dna[y + 2][x] &&
      dna[y][x] == dna[y + 3][x]
    ? 1
    : 0;

  let md = isElementUsed(exclude[2], x, y)
    ? 0
    : y >= dna.length - 3 || x >= dna.length - 3
    ? 0
    : dna[y][x] == dna[y + 1][x + 1] &&
      dna[y][x] == dna[y + 2][x + 2] &&
      dna[y][x] == dna[y + 3][x + 3]
    ? 1
    : 0;

  if (mh == 1) exclude[0].push([x, y], [x + 1, y], [x + 2, y], [x + 3, y]);
  if (mv == 1) exclude[1].push([x, y], [x, y + 1], [x, y + 2], [x, y + 3]);
  if (md == 1)
    exclude[2].push([x, y], [x + 1, y + 1], [x + 2, y + 2], [x + 3, y + 3]);

  return [exclude, mh, mv, md];
};

module.exports.isValid = function (dna) {
  if (utils.isEmpty(dna)) return false;
  for (let i = 0; i < dna.length; i++) {
    if (dna[i].length != dna.length) return false;

    if (
      dna[i]
        .split("A")
        .join("")
        .split("T")
        .join("")
        .split("C")
        .join("")
        .split("G")
        .join("").length > 0
    )
      return false;
  }
  return true;
};
